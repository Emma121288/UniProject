import re
import configparser

config = configparser.ConfigParser()
config.read('config.ini')

def validate_environment_name(env_name):
    env_name_regex = re.compile(config['environment_naming']['regex'])
    return bool(env_name_regex.match(env_name))

example_environment_names = ["sec-001", "dev-002", "qa-001", "908-new-environment"]

for env_name in example_environment_names:
    if validate_environment_name(env_name):
        print(f"Valid environment name: {env_name}")
    else:
        print(f"Invalid environment name: {env_name}")

